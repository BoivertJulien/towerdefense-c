#include "MyControlEngine.h"
#include "Entity/Entity.h"

/* On cherche le conteneur cibl� par le clic et celui-ci traitera l'action � r�aliser */
void MyControlEngine::MouseCallback(int button, int state, int x, int y) {
	if (*cinematicPlaying) {
		//si la cinematique est en cours, on ne gere pas les clics
		return;
	}
	//Calcul de la position du clic, relativement a la taille de l'ecran
	float relativeX = (x - (float)(*windowWidth/2)) / (*windowWidth / 2);
	float relativeY = -(y - (float)(*windowHeight / 2)) / (*windowHeight / 2); // a inverser car le l'axe y est invers� 

	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {
		for (auto &i : *contener) {
			if (i->receiveClic(relativeX,relativeY)) {
				i->actionClic(relativeX, relativeY);
			}
		}
	}
}