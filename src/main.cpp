#include <iostream>
#include "Engine.h"

#include "Entity/Vaisseau/Vaisseau.h"

#include "Entity/Asteroid/AsteroidBasic.h"
#include "Entity/Asteroid/AsteroidDust.h"
#include "Entity/Asteroid/AsteroidArmored.h"

#include "Entity/Missile/Missile.h"

#include "Container/BottomPadding.h"
#include "Container/TopPadding.h"
#include "Container/GameField.h"

#include "MyGraphicEngine.h"
#include "MyGameEngine.h"
#include "MyControlEngine.h"


int main(int argc, char * argv[])
{
	/* Variables d'etats (globales) */
	int manche = 1;
	bool running = false;
	int money = 2500;
	int wallLife = 100;
	int selectedTower = 1;
	int windowWidth = 800;
	int windowHeight = 600;
	int cinematicPlaying = 0; // 0 non, -1 defaite, +1 victoire
	int timeResultDisplay=2048;
    Engine e(argc,argv, windowWidth, windowHeight,"TowerDef");

	std::vector<Container *> contener;
	contener.push_back(new BottomPadding(&manche,&running,&money,&wallLife, &selectedTower));
	contener.push_back(new TopPadding(&manche,&running,&money, &wallLife,&selectedTower));
	contener.push_back(new GameField(&manche,&running,&money, &wallLife,&selectedTower, &cinematicPlaying, &timeResultDisplay));

    GraphicEngineBase * ge = new MyGraphicEngine(&contener, &windowWidth, &windowHeight, &cinematicPlaying);
    GameEngineBase * gme = new MyGameEngine(&contener);
    ControlEngineBase * ce = new MyControlEngine(&contener, &windowWidth, &windowHeight,&cinematicPlaying);
    
	e.setGraphicEngine(ge);
    e.setGameEngine(gme);
	e.setControlEngine(ce);
    
    e.start();

    return 0;
}