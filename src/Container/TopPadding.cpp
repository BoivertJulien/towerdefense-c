#include "TopPadding.h"


/* Affichage des �l�ments du TopPadding */
void TopPadding::draw()
{
	char wave[48];
	char cash[48];
	char life[48];
	//Affichage du Top Padding : (zone du container)
	GraphicPrimitives::drawFillRect2D(x, y, width, height, 0.1f, 0.1f, 0.1f, 0.5f);
	GraphicPrimitives::drawText2D(title, x + width / 10, y + height / 2, 0.2f, 0.6f, 0.8f);
	//Affichage des variables du jeu (argent, vie, numero de manche)
	strcpy(wave, "WAVE : ");
	strcpy(cash, "MONEY : $");
	strcpy(life, "LIFE : ");
	GraphicPrimitives::drawText2D(strcat(wave,mancheStr), x+width/2,y+height/2, 0.25f, 0.25f, 0.9f);
	GraphicPrimitives::drawText2D(strcat(cash,moneyStr), x + width / 10, y + height / 6, 0.25f, 0.25f, 0.9f);
	//Affichage de la vie selon 3 �tats de danger
	if (*wallLife <= 25) GraphicPrimitives::drawText2D(strcat(life, wallLifeStr), x + width / 2, y + height / 6, 0.8f, 0.25f, 0.3f);
	else if (*wallLife <= 50) GraphicPrimitives::drawText2D(strcat(life,wallLifeStr), x + width / 2, y + height / 6, 0.6f, 0.25f, 0.4f);
	else GraphicPrimitives::drawText2D(strcat(life, wallLifeStr), x + width / 2, y + height / 6, 0.25f, 0.25f, 0.9f);

	//Si une vague est en cours, on l'affiche
	if (*running) {
		GraphicPrimitives::drawText2D("Vague En Cours ..", x + (width / 4)*3, y + height / 6, 0.25f, 0.25f, 0.9f);
	} //sinon afficher sauvegarde menu
}

/* Mise a jour des strings du TopPadding (contenant les varibles du jeu) */
void TopPadding::update()
{
	setWallLifeStr();
	setMancheStr();
	setMoneyStr();
	return;
}

/* Un clic sur TopPadding n'est pas trait� */
void TopPadding::actionClic(float x, float y)
{
	return;
}

/* Mise a jour pour chaques variables de jeu */
void TopPadding::setWallLifeStr() {
	delete[] wallLifeStr;
	wallLifeStr = valToString(*wallLife);
}
void TopPadding::setMancheStr() {
	delete[] mancheStr;
	mancheStr = valToString(*manche);
}
void TopPadding::setMoneyStr() {
	delete[] moneyStr;
	moneyStr = valToString(*money);
}

/* Obtenir char* a partir d'un int */
char * TopPadding::valToString(int val) {
	std::string s = std::to_string(val);
	char *cstr = new char[s.length() + 1];
	strcpy(cstr, s.c_str());
	return cstr;  //use char const* as target type
}