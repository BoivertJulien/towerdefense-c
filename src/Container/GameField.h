#pragma once
#include "Container.h"
#include "Engine.h"
#include "../MyGraphicEngine.h"
#include "Case\BattleFieldCase.h"
#include "../Entity/Entity.h"

#include "../Entity/Vaisseau/VaisseauBasic.h"
#include "../Entity/Vaisseau/VaisseauSwift.h"
#include "../Entity/Vaisseau/VaisseauPowerful.h"

#include "../Entity/Asteroid/AsteroidBasic.h"
#include "../Entity/Asteroid/AsteroidDust.h"
#include "../Entity/Asteroid/AsteroidArmored.h"

#include "../Entity/Missile/Missile.h"
#include <fstream>
#include <iostream>
#include <string>
#include <algorithm>

// frequence d'apparition des ennemis (en nombre de tic)
#define FREQ_MONSTRE_1_10 200
#define FREQ_MONSTRE_11_20 140
#define FREQ_MONSTRE_21_30 100
#define SHOW_RESULT_TIC 2048
#define CHECKSUM_VALUE 506657375

struct Triplet{
	int rang;
	int type;
	int yIndex;
};

class GameField : public Container
{
public:
	//Attributs propres a GameField
		// Dimension du Damier
		const int ligne = 7;
		const int colonne = 10;
		// Dimension d'une Case
		float decoupageWidth;
		float decoupageHeight;
		//Varible decr�mentant depuis  FREQ_MONSTRE. Si =0, apparition d'un nouvel ennemi
		int freqMonstre;

		// Vector contenant les Cases et les Entit�s du Jeu
		std::vector<BattleFieldCase * > cases;
		std::vector<Missile *> missiles;
		std::vector<Vaisseau *> ships;
		std::vector<Asteroid *> asteroids;
		// Position en y des ennemis du niveau courant
		std::vector<Triplet *> asteroidsLevel;
		// Position en y du centre de chaques lignes
		std::vector<float> linesCoord;
		
		std::string levelFile;

		int * cinematicPlaying;
		int * timeResultDisplay;

	// Constructeurs et Destructeurs
	GameField(int * manche_,bool * running_,int * money_, int * wallLife_, int * selectedTower_, int * cinematicPlaying_, int * timeResultDisplay_):
		Container(-1.0f,-0.6f,2.0f,1.4f,manche_,running_,money_,wallLife_, selectedTower_), freqMonstre(FREQ_MONSTRE_1_10), levelFile("src/file/Level.txt"), 
		cinematicPlaying(cinematicPlaying_), timeResultDisplay(timeResultDisplay_)
	{
		decoupageWidth = width / colonne;
		decoupageHeight = height / ligne;
		// On ajoute au vector de Case, ligne*colonne= 70 cases de type BattleFieldCase
		for (int l = 0; l < ligne; l++) {
			for (int c = 0; c < colonne; c++) {
				cases.push_back( new BattleFieldCase(
						x + decoupageWidth*c,
						y + decoupageHeight*l,
						decoupageWidth,
						decoupageHeight
				));
			}
			//On ajoute le centre de chaque ligne au vecteur 'linesCoord'
			linesCoord.push_back(y+decoupageHeight*l+ decoupageHeight/2);
		}
		reverse(linesCoord.begin(),linesCoord.end());

		std::ifstream myfile(levelFile);
		uint32_t crc = checksum(myfile);

		if (crc != CHECKSUM_VALUE) {
			std::cout << "Logiciel Corrompu";
			exit(0);
		}
		//On g�n�re le premier niveau (toujours g�n�r� en amont de la phase pr�paration (phase ou le joueur pose ses tours))
		generateLevel(1);
	}

	~GameField() {
		clearCasesMemory();
		clearMissilesMemory();
		clearShipsMemory();
		clearAsteroidsMemory();
		clearAsteroidsLevelMemory();
	}

	// Methodes abstraites h�rit�es � redefinir
	void draw();
	void update();
	void actionClic(float x, float y);
	void initParty();

	// Methodes Propres
	void generateLevel(int lvl);
	bool timeToAddEnnemies();
	void addEnnemies();

	bool levelHasEnded();
	void levelTerminate();
	void treatCollisionAsteroidMissile();
	void treatCollisionAsteroidTower();
	uint32_t checksum(std::ifstream& file);

	void clearCasesMemory();
	void clearMissilesMemory();
	void clearShipsMemory();
	void clearAsteroidsMemory();
	void clearAsteroidsLevelMemory();

};
