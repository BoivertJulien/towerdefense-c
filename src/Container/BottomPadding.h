#pragma once
#include "Container.h"
#include "Case/SelectionTowerCase.h"
#include "Case/RunVagueCase.h"

class BottomPadding : public Container
{
public:
	// Attributs propres � la classe BottomContainer
	std::vector<SelectionTowerCase * > cases; //Case de Selection de Tour a poser 
	RunVagueCase * startCase;

	// Constructeurs et Destructeurs
	BottomPadding(int * manche_,bool * running_,int * money_, int * wallLife_, int * selectedTower_) :
		Container(-1.0f, -1.0f, 2.0f, 0.4f,manche_,running_,money_,wallLife_, selectedTower_)
	{
		//Case "Selection Tour"
		cases.push_back(new SelectionTowerCase(-0.5f, -0.95f, 0.3f, 0.3f,1));
		cases.push_back(new SelectionTowerCase(-0.2f, -0.95f, 0.3f, 0.3f,2));
		cases.push_back(new SelectionTowerCase(0.1f, -0.95f, 0.3f, 0.3f,3));
		//Case "Start Vague"
		startCase = new RunVagueCase(0.6f, -0.9f, 0.3f, 0.2f);
	}
	~BottomPadding() {
		delete startCase;
		delete cases[0];		
		delete cases[1];
		delete cases[2];
	}

	// Methodes h�rit�es � redefinir
	void draw();
	void update();
	void actionClic(float x, float y);

};

