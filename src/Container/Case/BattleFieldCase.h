#pragma once
#include "Case.h"

class BattleFieldCase : public Case
{
public:
	//Attribut :
	bool tower; // Une Case est-elle occup� par une tour

	//Constructeur et Destructeur
	BattleFieldCase(float x_, float y_, float w_, float h_) :
		Case(x_, y_, w_, h_, 0.1f, 0.1f, 0.1f), tower(false) {}
	~BattleFieldCase(){}

	// Methodes propre au BattleFieldCase
	float getTower(){return tower;}
	void affectedTower(bool b){tower = b;}
	void towerDie() { tower = false; }
};

