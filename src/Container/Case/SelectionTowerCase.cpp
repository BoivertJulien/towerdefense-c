#include "SelectionTowerCase.h"


/*
Dessine la case de Selection. 
*/
void SelectionTowerCase::draw(int t)
{
	//Si le type de tour courament s�lectionn� (t) correspond a celle d�finit par cette Case, on dessine un cadre autour de la case
	if (type == t) {
		GraphicPrimitives::drawFillRect2D(x + 0.01f, y + 0.01f, width - 0.02f, height - 0.02f, red, green, blue, alpha);
		GraphicPrimitives::drawOutlinedRect2D(x + 0.01f, y + 0.01f, width - 0.02f, height - 0.02f, 0.0f, 1.0f, 1.0f, alpha);
	}
	else {
		GraphicPrimitives::drawFillRect2D(x + 0.01f, y + 0.01f, width - 0.02f, height - 0.02f, red, green, blue, alpha);
	}
	shipToShow->drawVaisseau();
	GraphicPrimitives::drawText2D(cost, x + width / 10, y + height / 6, 0.1f, 0.1f, 0.9f);

}
