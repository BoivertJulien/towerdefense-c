#pragma once
#include "Case.h"

/*
Classe servant juste de zone cliquable pour lancer une partie
*/
class RunVagueCase : public Case
{
public:
	RunVagueCase(float x_, float y_, float w_, float h_) :
		Case(x_, y_, w_, h_, 0.5f, 0.5f, 0.8f) {}
	~RunVagueCase(){}
	void draw();
};