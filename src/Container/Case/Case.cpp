#include "Case.h"

/* On dessine une Case par un rectangle uni */
void Case::draw() {
	GraphicPrimitives::drawFillRect2D(x+0.01f, y+0.01f, width-0.02f, height-0.02f, red, green, blue, alpha);
}

// Methodes h�rit�es abstraites qui ne sont pas utilis�es pour les Cases
void Case::update(){return;}
void Case::actionClic(float x, float y){return;}
