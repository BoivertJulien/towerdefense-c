#pragma once
#include "../Container.h"

class Case : public Container {

public:
	float red, green, blue, alpha;
	 
	//Constructeur additionnel car les variables d'etat ne sont pas necessaire pour les Cases
	Case(float x_, float y_, float width_, float height_, float r_, float g_, float b_) :
		Case(x_, y_, width_, height_, r_, g_, b_, NULL, NULL, NULL, NULL, NULL) {}
	
	Case(float x_, float y_, float width_, float height_, float r_ ,float g_,float b_,int * manche_, bool * running_,int * money_, int * wallLife_, int * selectedTower_) : 
		Container(x_, y_, width_, height_,manche_,running_,money_,wallLife_,selectedTower_),
		red(r_), green(g_), blue(b_), alpha(1.0f) {}

	// Methodes abstraites h�rit�s a redefinir
	void draw();
	void update();
	void actionClic(float x, float y);
};