#pragma once
#include "Case.h"
#include "../../Entity/Vaisseau/Vaisseau.h"
#include "../../Entity/Vaisseau/VaisseauBasic.h"
#include "../../Entity/Vaisseau/VaisseauSwift.h"
#include "../../Entity/Vaisseau/VaisseauPowerful.h"

class SelectionTowerCase : public Case
{
public:
	// Attribut :
	int type; // Type de tour d�sign� par la case courante
	Vaisseau * shipToShow;
	char* cost;

	//Constructeurs et Destructeur
	SelectionTowerCase(float x_, float y_, float w_, float h_, int type_) :
		Case(x_, y_, w_, h_, 0.4f, 0.2f, 0.7f), type(type_)
	{
		switch (type)
		{
		case 1:
			shipToShow = new VaisseauBasic(x + width / 2, y + height / 2, nullptr, nullptr, nullptr);
			cost = "$500";
			break;
		case 2:
			shipToShow = new VaisseauSwift(x + width / 2, y + height / 2, nullptr, nullptr, nullptr);
			cost = "$1000";
			break;
		case 3:
			shipToShow = new VaisseauPowerful(x + width / 2, y + height / 2, nullptr, nullptr, nullptr);
			cost = "$3000";
			break;
		}
	}

	~SelectionTowerCase() {
		delete shipToShow;
		delete cost;
	}

	// Methodes propres aux SelectionTowerCase
	void draw(int t);
	int getType() {
		return type;
	}
};