#include "RunVagueCase.h"

void RunVagueCase::draw() {
	GraphicPrimitives::drawFillRect2D(x + 0.01f, y + 0.01f, width - 0.02f, height - 0.02f, red, green, blue, alpha);
	GraphicPrimitives::drawText2D("START", x + width / 10, y + height / 6, 0.1f, 0.1f, 0.9f);
}