#include "GameField.h"
#include <set>
#include <iostream>
#include <fstream>
#include <string>
#include <tuple>
#include <thread>
#include <chrono>

/* Affichage des entit�es du jeu */
void GameField::draw() {
	unsigned int i;
	GraphicPrimitives::drawFillRect2D(x, y, width, height, 0.2f, 0.2f, 0.9f, 0.5f);
	for (i = 0; i < cases.size(); i++) {
		(cases)[i]->draw();
	}
	for (i = 0; i < ships.size(); i++) {
		(ships)[i]->draw();
	}
	for (i = 0; i < asteroids.size(); i++) {
		(asteroids)[i]->draw();
	}
	for (i = 0; i < missiles.size(); i++) {
		(missiles)[i]->draw();
	}
}

/* Mise a jour du jeu (mise a jour des entit�s du jeu) */
void GameField::update() {
	if (*cinematicPlaying != 0) {
		*timeResultDisplay = *timeResultDisplay - 1;
		if (*timeResultDisplay == 0) {
			initParty();
		}
		return;
	}
	if (*running) {
		// Ajout d'ennemies si compteur a 0 et si reste ennemies a g�n�rer
		freqMonstre--;
		if (timeToAddEnnemies()) {
			addEnnemies();
		}
		unsigned int i;
		// Mise a jour des entit�s du jeu
		for (i = 0; i < ships.size(); i++) {
			(ships)[i]->tick();
		}
		for (i = 0; i < asteroids.size(); i++) {
			(asteroids)[i]->tick();
		}
		for (i = 0; i < missiles.size(); i++) {
			(missiles)[i]->tick();
		}
		// Traitement des collisions
		treatCollisionAsteroidMissile();
		treatCollisionAsteroidTower();

		//Traitement eventuelle fin de partie
		if (*wallLife <= 0) {
			// le joueur vient de mourrir si la cinematique n'est pas en cours
			if (*cinematicPlaying == 0) {
				*cinematicPlaying = -1;
				*running = false;
				*timeResultDisplay = SHOW_RESULT_TIC;
			}
		}
		// Traitement fin du niveau
		if (levelHasEnded()) {
			levelTerminate();
		}
	}
	else return;
}

bool GameField::timeToAddEnnemies() {
	return (freqMonstre <= 0 && !asteroidsLevel.empty());
}

void GameField::addEnnemies() {
	if (*manche < 11) freqMonstre = FREQ_MONSTRE_1_10;
	else if (*manche < 21) freqMonstre = FREQ_MONSTRE_11_20;
	else freqMonstre = FREQ_MONSTRE_21_30;

	int currentRang = (asteroidsLevel)[0]->rang;
	while (!asteroidsLevel.empty() && (asteroidsLevel)[0]->rang == currentRang) {
		switch ((asteroidsLevel)[0]->type) {
		case 1: {
			asteroids.push_back(new AsteroidBasic((linesCoord)[(asteroidsLevel)[0]->yIndex]));
			break;
		}
		case 2: {
			asteroids.push_back(new AsteroidDust((linesCoord)[(asteroidsLevel)[0]->yIndex]));
			break;
		}
		case 3: {
			asteroids.push_back(new AsteroidArmored((linesCoord)[(asteroidsLevel)[0]->yIndex]));
			break;
		}
		}
		delete *asteroidsLevel.begin();
		asteroidsLevel.erase(asteroidsLevel.begin());
	}
}

bool GameField::levelHasEnded() {
	return (asteroids.empty() && asteroidsLevel.empty());
}

void GameField::levelTerminate() {
	for (unsigned int i = 0; i < ships.size(); i++){
		ships[i]->rePlaceForBattle();
	}
	clearMissilesMemory();
	missiles.clear();
	*running = false;
	*money += (*manche * 50);
	*manche += 1;
	*wallLife += 10;
	generateLevel(*manche);
}

/* Clic sur le Damier */
void GameField::actionClic(float x, float y) {
	if (!*running) {
		for (unsigned int i = 0; i < cases.size(); i++) {
			//Si fond suffisant et case libre, ajout de la tour a la case
			if ((cases)[i]->receiveClic(x, y) && !(cases)[i]->getTower() && *money >= 500) {
				if (*selectedTower == 1) {
					ships.push_back(new VaisseauBasic((cases)[i]->getX(), (cases)[i]->getY(), &missiles, (cases)[i], &asteroids));
					(cases)[i]->affectedTower(true);
					*money -= 500;
				}
				else if (*selectedTower == 2 && *money >= 1000) {
					ships.push_back(new VaisseauSwift((cases)[i]->getX(), (cases)[i]->getY(), &missiles, (cases)[i], &asteroids));
					(cases)[i]->affectedTower(true);
					*money -= 1000;
				}
				else if (*selectedTower == 3 && *money >= 3000) {
					ships.push_back(new VaisseauPowerful((cases)[i]->getX(), (cases)[i]->getY(), &missiles, (cases)[i], &asteroids));
					(cases)[i]->affectedTower(true);
					*money -= 3000;
				}
				return;
			}
		}
	}
	else return;
}

void GameField::initParty()
{
	// On recommence la partie en modifiant les variables de jeu
	*running = false;
	*money = 2500;
	*manche = 1;
	*wallLife = 100;
	clearShipsMemory();
	ships.clear();
	clearAsteroidsMemory();
	asteroids.clear();
	clearMissilesMemory();
	missiles.clear();
	generateLevel(*manche);
	*cinematicPlaying = 0;
	*timeResultDisplay = SHOW_RESULT_TIC;
	// on reinitialise les cases de combat pour que celles-ci se consid�rent comme libre (pas occup� par des vaisseau)
	for (unsigned int i = 0; i<cases.size(); i++) {
		(cases)[i]->towerDie();
	}
}




///////////////////////////////////////////////////////////
/////////////	Traitements des Impacts ///////////////////
///////////////////////////////////////////////////////////


/* Traitement des sorties de missiles + impact Missiles -> Asteroids */
void GameField::treatCollisionAsteroidMissile() {
	// Liste des indexe a retir� pour eviter la modif de la liste en // de son parcours
	std::set<int> asteroidIndexToRemove;
	std::set<int> missileIndexToRemove;

	for (unsigned int j = 0; j < missiles.size(); j++) {
		if ((missiles)[j]->touchBorder()) {
			missileIndexToRemove.insert(j);
		}
	}
	//Verification impact Missile - Asteroid
	for (unsigned int i = 0; i < asteroids.size(); i++) {
		for (unsigned int j = 0; j < missiles.size(); j++) {
			if ((asteroids)[i]->receiveImpact((missiles)[j])) {

				(asteroids)[i]->actionAtImpact((missiles)[j]->getDamage());
				(missiles)[j]->hadHit();
				 
				if ((missiles)[j]->getLife() <= 0) {
					missileIndexToRemove.insert(j);
				}
			}
		}
		if ((asteroids)[i]->getLife() <= 0) {
			asteroidIndexToRemove.insert(i);
		}
	}
	while (!asteroidIndexToRemove.empty()) {
		delete * (asteroids.begin() + *asteroidIndexToRemove.rbegin());
		asteroids.erase(asteroids.begin() + *asteroidIndexToRemove.rbegin());
		asteroidIndexToRemove.erase(*asteroidIndexToRemove.rbegin());
	} 
	while (!missileIndexToRemove.empty()) {
		delete * (missiles.begin() + *missileIndexToRemove.rbegin());
		missiles.erase(missiles.begin() + *missileIndexToRemove.rbegin());
		missileIndexToRemove.erase(*missileIndexToRemove.rbegin());
	}
}


/* Traitement des collision sur le mur + impact Asteroid -> Tour */
void GameField::treatCollisionAsteroidTower() {
	// Liste des indexe a retir� pour eviter la modif de la liste en // de son parcours
	std::set<int> shipIndexToRemove;
	std::set<int> asteroidIndexToRemove2;

	//Verification de l'impact des asteroids dans le mur
	for (unsigned int j = 0; j < asteroids.size(); j++) {
		if ((asteroids)[j]->touchBorder()) {
			asteroidIndexToRemove2.insert(j);
			*wallLife -= (asteroids)[j]->getDamage();
		}
	}
	//Verification impact Vaisseau - Asteroid
	for (unsigned int i = 0; i < ships.size(); i++) {
		for (unsigned int j = 0; j < asteroids.size(); j++) {
			if ((ships)[i]->receiveImpact((asteroids)[j])) {

				(ships)[i]->actionAtImpact((asteroids)[j]->getDamage());
				(asteroids)[j]->hadHit();

				if ((asteroids)[j]->getLife() <= 0) {
					asteroidIndexToRemove2.insert(j);
				}
			}
		}
		if ((ships)[i]->getLife() <= 0) {
			(ships)[i]->getCaseRef()->towerDie();
			shipIndexToRemove.insert(i);
		}
	}
	while (!asteroidIndexToRemove2.empty()) {
		delete * (asteroids.begin() + *asteroidIndexToRemove2.rbegin());
		asteroids.erase(asteroids.begin() + *asteroidIndexToRemove2.rbegin());
		asteroidIndexToRemove2.erase(*asteroidIndexToRemove2.rbegin());
	}
	while (!shipIndexToRemove.empty()) {
		delete * (ships.begin() + *shipIndexToRemove.rbegin());
		ships.erase(ships.begin() + *shipIndexToRemove.rbegin());
		shipIndexToRemove.erase(*shipIndexToRemove.rbegin());
	}
}

//Genere la liste des position en y, des Asteroides
void GameField::generateLevel(int lvl) {
	using namespace std;
	string line;
	ifstream myfile(levelFile);

	if (myfile.is_open())
	{
		if (lvl != -1) {
			for (int i = 0; i < lvl; i++) {
				if (!getline(myfile, line)) {
					//On a parcouru tout les niveaux, je jeu est termin� (partie gagn�e)
					line = "";
					*cinematicPlaying = 1;
					*running = false;
					return;
				}
			}
		}

		clearAsteroidsLevelMemory();
		asteroidsLevel.clear();
		for (unsigned int i = 0; i < line.length(); i+=3) {
			Triplet *t= new Triplet();
			t->rang = line[i]-'0';
			t->type = line[i + 1]-'0';
			t->yIndex = line[i + 2]-'0';
			asteroidsLevel.push_back(t);
		}
		myfile.close();
	}

	else {
		cout << "Fichier Introuvable.";
		exit(0);
	}
}

// Prit a : https://codereview.stackexchange.com/questions/104948/32-bit-checksum-of-a-file
uint32_t GameField::checksum(std::ifstream& file)
{
	uint32_t checksum = 0;
	unsigned shift = 0;
	for (uint32_t ch = file.get(); file; ch = file.get()) {
		checksum += (ch << shift);
		shift += 8;
		if (shift == 32) {
			shift = 0;
		}
	}
	return checksum;
}



void GameField::clearCasesMemory() {
	for (unsigned int i = 0; i < cases.size(); i++) { delete cases[i]; }
}
void GameField::clearMissilesMemory() {
	for (unsigned int i = 0; i < missiles.size(); i++) { delete missiles[i]; }
}

void GameField::clearShipsMemory() {
	for (unsigned int i = 0; i < ships.size(); i++) { delete ships[i]; }
}

void GameField::clearAsteroidsMemory() {
	for (unsigned int i = 0; i < asteroids.size(); i++) { delete asteroids[i]; }
}
void GameField::clearAsteroidsLevelMemory() {
	for (unsigned int i = 0; i < asteroidsLevel.size(); i++) { delete asteroidsLevel[i]; }
}