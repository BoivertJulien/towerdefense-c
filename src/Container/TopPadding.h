#pragma once
#include "Container.h"
#include<string>

class TopPadding : public Container
{
public:
	// Attributs propre � la classe TopPadding
	char * mancheStr;
	char * title;
	char * moneyStr;
	char * wallLifeStr;

	// Constructeurs Destructeurs
	TopPadding(int * manche_, bool * running_,int * money_,int * wallLife_,int * selectedTower_) : 
		Container(-1.0f, 0.8f, 2.0f, 0.2f, manche_, running_,money_,wallLife_, selectedTower_),
		title(new char[10] {'T','0','W','3','R',' ','D','E','F','\0'})
		{	setMancheStr();
			setMoneyStr();
			setWallLifeStr();
		}
	~TopPadding() {
		delete [] mancheStr;
		delete [] title;
		delete [] moneyStr;
		delete [] wallLifeStr;
	}

	// Methodes h�rit�es a re-definir
	void draw();
	void update();
	void actionClic(float x, float y);

	// Methodes propres � la classe TopPadding
	void setWallLifeStr();
	void setMancheStr();
	void setMoneyStr();
	char * valToString(int val);
};