#include "BottomPadding.h"


/* Affichage du BottomPadding */
void BottomPadding::draw()
{
	// Affichage de la zone du Bottom Padding
	GraphicPrimitives::drawFillRect2D(x, y, width, height, 0.05f, 0.05f, 0.1f, 1.0f);

	// Si la manche est en cours, on affiche seulement "Vague en cours"
	if (*running) {
		GraphicPrimitives::drawText2D("Vague En Cours ..", x + width / 2, y + height / 2, 0.1f, 0.1f, 0.9f);
	}
	//Sinon on affiche les 'Container's de Selection de Tour et celui de Lancement de Partie
	else {
		for (unsigned int i = 0; i < cases.size(); i++) {
			(cases)[i]->draw(*selectedTower);
		}
		startCase->draw();
	}
}

/* Le BottomPadding ne se met pas a jour */
void BottomPadding::update()
{
	return;
}

/* Action en cas de clic */
void BottomPadding::actionClic(float x, float y)
{
	// Si la partie n'est pas en cours, un clic :
	if (!*running) {
		// 1) peut choisir le type de tour a poser
		for (unsigned int i = 0; i < cases.size(); i++) {
			if ((cases)[i]->receiveClic(x, y)) {
				*selectedTower = (cases)[i]->getType();
				return;
			}
		}
		// 2) Lancer une partie
		if (startCase->receiveClic(x, y)) {
			*running = true;
			return;
		}
	}
	//Si la partie est en cours, un clic ne fait rien
	else return;
}