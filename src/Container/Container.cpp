#include "Container.h"


/* 
Retourne booleen selon si un clic touche ce Container.
Le clic (x et y), est pass�, deja calcul� relativement � la taille de la fenetre.
*/
bool Container::receiveClic(float x_, float y_)
{
	return ((x_ > x && x_ < x + width) && (y_ > y && y_ < y + height));
}

float Container::getX()
{
	return x + width / 2;
}

float Container::getY()
{
	return y + height / 2;
}