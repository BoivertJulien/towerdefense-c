#pragma once
#include "GraphicPrimitives.h"

class Container
{
public :
	// Attributs communs aux Container
	float x, y, width, height; // Pour les Container, x et y indique la position du coin Bas-Gauche du conteneur
	int * manche;
	int * money;
	bool * running;
	int * wallLife;
	int * selectedTower;

	// Constructeurs et Destructeurs
	Container(float x_, float y_, float w, float h,int * manche_,bool * running_,int * money_,int * wallLife_,int * selectedTower_) : 
		x(x_), y(y_), width(w), height(h),manche(manche_),running(running_),money(money_),wallLife(wallLife_),selectedTower(selectedTower_)
	{}
	~Container() {
		// ici, les pointeurs r�f�rencent des valeurs globales ont ne doit pas supprimer leur contenu
		// (Destruction fin du main)
	}

	// Methodes abstraites � redefinir
	virtual void draw()=0;
	virtual void update() = 0;
	virtual void actionClic(float x, float y)=0;

	// Methodes communes aux classes h�ritant de Container
	bool receiveClic(float x_, float y_);
	float getX();
	float getY();

};
