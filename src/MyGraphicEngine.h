#pragma once
#include "Engine.h"
#include "GraphicPrimitives.h"
#include "Container/Container.h"

//LOOSE PATTERN
#define XC_L { -0.8f, -0.7f,-0.5f, -0.6f,-0.3f, -0.4f }
#define YC_L { -0.6f, 0.7f, 0.8f,-0.4f, -0.4f,-0.6f }
#define XC_O1 { -0.3f, -0.2f,0.f, -0.1f }
#define YC_O1 { -0.3f, 0.f, -0.3f,-0.6f }
#define XC_O2 { 0.f, 0.1f,0.3f, 0.2f }
#define YC_O2 { -0.3f, 0.f, -0.3f,-0.6f }
#define XC_S { 0.2f, 0.4f,0.5f, 0.3f, 0.5f, 0.4f,0.3f, 0.4f }
#define YC_S { 0.f, 0.2f, 0.1f,0.f,-0.3f, -0.7f,-0.6f,-0.3f }
#define XC_E { 0.5f, 0.8f,0.9f, 0.6f,0.7f, 0.6f ,0.9f,0.8f,0.5f,0.6f}
#define YC_E { -0.2f, 0.f,-0.1f, -0.2f,-0.3f, -0.5f ,-0.6f,-0.7f,-0.6f,-0.3f}
//	WIN PATTERN
#define XC_W { -0.8f, -0.6f,-0.6f, -0.3f, -0.2f, -0.1f,0.1f, -0.1f,-0.3f,-0.7f }
#define YC_W { 0.7f, 0.6f,-0.3f, -0.1f, -0.2f, 0.5f,0.6f, -0.5f,-0.3f,-0.5f }
#define XC_I { 0.1f, 0.2f,0.1f, 0.f }
#define YC_I { 0.1f, 0.2f, -0.5f,-0.4f }
#define XC_N { 0.2f, 0.3f,0.5f, 0.6f,0.7f, 0.6f ,0.5f,0.4f,0.3f}
#define YC_N { -0.4f, 0.3f,0.f, 0.4f,0.4f, -0.5f ,-0.4f,0.f,-0.5f}

class MyGraphicEngine:public GraphicEngineBase {
	// Attributs :
	std::vector<Container * > *contener; // Pointeur vers le vector contenant les références vers les Conteneurs
	//Taille courante de l'ecran
	int * windowWidth;
	int * windowHeight;
	int * cinematicPlaying;

	std::vector<float> xC_L = XC_L;
	std::vector<float> yC_L = YC_L;
	std::vector<float> xC_O1 = XC_O1;
	std::vector<float> yC_O1 = YC_O1;
	std::vector<float> xC_O2 = XC_O2;
	std::vector<float> yC_O2 = YC_O2;
	std::vector<float> xC_S = XC_S;
	std::vector<float> yC_S = YC_S;
	std::vector<float> xC_E = XC_E;
	std::vector<float> yC_E = YC_E;
	std::vector<float> xC_W = XC_W;
	std::vector<float> yC_W = YC_W;
	std::vector<float> xC_I = XC_I;
	std::vector<float> yC_I = YC_I;
	std::vector<float> xC_N = XC_N;
	std::vector<float> yC_N = YC_N;

public:

    MyGraphicEngine(std::vector<Container * > * contener_, int * windowWidth_,int * windowHeight_, int * cinematicPlaying_):
		contener(contener_), windowWidth(windowWidth_), windowHeight(windowHeight_), 
		cinematicPlaying(cinematicPlaying_)
	{
	}
    
    virtual void Draw();
	virtual void reshape(int width, int height);
	void showCinematicWin();
	void showCinematicLoose();
};
