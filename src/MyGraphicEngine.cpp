#include "MyGraphicEngine.h"

/* On dessine chaque conteneur qui dessinera lui meme ses composants */
void MyGraphicEngine::Draw(){
	if (*cinematicPlaying) {
		if (*cinematicPlaying == 1) {
			showCinematicWin();
			return;
		}
		else {
			showCinematicLoose();
			return;
		}
	}
	for (unsigned int i = 0; i < contener->size(); i++) {
		(*contener)[i]->draw();
	}
}

//On met a jour les dimensions de l'ecran. Cela permettra le calcul relatif de la position d'un clic selon la taille de l'ecran.
void MyGraphicEngine::reshape(int width, int height)
{
		*windowWidth = width;
		*windowHeight = height;
}
void MyGraphicEngine::showCinematicWin() {
	float tmp;
	for (int i = 1; i < 256; i++) {
		tmp = (1.f / i);
		GraphicPrimitives::drawFillRect2D(-tmp, -tmp, tmp*2,tmp*2, tmp,tmp/2,tmp/2);
	}
	GraphicPrimitives::drawOutlinedPolygone2D(xC_W, yC_W, 0., 0., 1.);
	GraphicPrimitives::drawOutlinedPolygone2D(xC_I, yC_I, 1., 1., 1.);
	GraphicPrimitives::drawOutlinedPolygone2D(xC_N, yC_N, 1., 0., 0.);
 }
void MyGraphicEngine::showCinematicLoose() {
	
	float tmp;
	for (int i = 1; i < 256; i++) {
		tmp = (1.f / i);
		GraphicPrimitives::drawFillRect2D(-tmp, -tmp,tmp*2,tmp*2, tmp / 2, tmp / 2, tmp);
	}
	GraphicPrimitives::drawOutlinedPolygone2D(xC_L, yC_L, 0., 1., 1.);
	GraphicPrimitives::drawOutlinedPolygone2D(xC_O1, yC_O1, 1., 1., 0.);
	GraphicPrimitives::drawOutlinedPolygone2D(xC_O2, yC_O2, 1., 0., 0.);
	GraphicPrimitives::drawOutlinedPolygone2D(xC_S, yC_S, 0., 0., 1.);
	GraphicPrimitives::drawOutlinedPolygone2D(xC_E, yC_E, 0., 1., 0.);
}
