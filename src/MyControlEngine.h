#pragma once

#include "Engine.h"

#include <stdlib.h>     /* srand, rand */
#include <time.h>	/* time */

#include "Container/Container.h"

class MyControlEngine:public ControlEngineBase {
	// Attributs :
	std::vector<Container * > *contener; // Pointeur vers le vector contenant les références vers les Conteneurs
	// Taille courante de l'ecran
	int * windowWidth;
	int * windowHeight;
	int * cinematicPlaying;

public:

    MyControlEngine(std::vector<Container * > * contener_, int * windowWidth_, int * windowHeight_, int * cinematicPlaying_):
		contener(contener_), windowWidth(windowWidth_), windowHeight(windowHeight_),
		cinematicPlaying(cinematicPlaying_)

	{
		srand(time(NULL));
	}
    
    virtual void MouseCallback(int button, int state, int x, int y) ;
};
