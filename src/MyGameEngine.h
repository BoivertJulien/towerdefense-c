#pragma once

#include "Engine.h"

#include "Container/Container.h"

class MyGameEngine:public GameEngineBase {
	//Attributs
	std::vector<Container * > *contener; // Pointeur vers le vector contenant les références vers les Conteneurs

public:

    MyGameEngine(std::vector<Container * > * contener_):
		contener(contener_)
	{}
    
    virtual void idle();
    
};