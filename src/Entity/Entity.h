#pragma once
#include<algorithm>
#include "GraphicPrimitives.h"

class Entity
{
public:
	float x, y, rayonBox, dX,dY,red,green,blue; // Pour les Entit�s, x et y indique la position du CENTRE de l'entit�e
	int life, damage;

	Entity(float x_, float y_, float rayonBox_, int life_,int damage_,float dX_,float dY_,float red_, float green_,float blue_){
		x = x_;
		y = y_;
		rayonBox = rayonBox_;
		life = life_; damage = damage_;
		dX = dX_; dY = dY_;
		red = red_; green = green_; blue = blue_;
	}

	virtual void draw()=0;
	virtual void tick()=0;
	virtual void actionAtImpact(int damage_)=0;
	virtual void hadHit()=0;

	bool receiveImpact(Entity * ent);
	float getDist(float x_, float y_);
	void drawLife(int initLifePoint);

	float getX()
	{
		return x;
	}
	float getY()
	{
		return y;
	}
	float getRayon()
	{
		return rayonBox;
	}
	int getDamage()
	{
		return damage;
	}
	int getLife()
	{
		return life;
	}

};
