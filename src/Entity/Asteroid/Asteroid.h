#pragma once
#include "../Entity.h"

class Asteroid : public Entity {

public:
	std::vector<float> xC;
	std::vector<float> yC;

	//Constructeurs pour sous classes
	Asteroid(float y_, float rayonBox_, int life_, int damage_, float dX_, float red_, float green_, float blue_, std::vector<float> xC_, std::vector<float> yC_) :
		Entity(1.0f, y_, rayonBox_, life_, damage_, dX_,0.0f, red_, green_, blue_),
		xC(xC_),yC(yC_)
	{
	}

	//Fonction abstract de la classe Entity
	void draw();
	void tick();
	void actionAtImpact(int damage_);
	void hadHit();
	void drawAsteroid();

	virtual int getLifeAtInit()=0;

	//Fonction perso
	bool touchBorder();

};