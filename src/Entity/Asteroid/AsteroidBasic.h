#pragma once
#include "Asteroid.h"

#define RAYON_BOX_ASTEROID_BASIC 0.05f
#define LIFE_ASTEROID_BASIC 70
#define DAMAGE_ASTEROID_BASIC 30
#define VITESSE_ASTEROID_BASIC -0.0006f
#define RED_ASTEROID_BASIC 0.5f
#define GREEN_ASTEROID_BASIC 0.1f
#define BLUE_ASTEROID_BASIC 0.1f
#define XC_ASTEROID_BASIC { -0.05f, -0.02f,0.0f, 0.02f,0.05f, 0.02f,0.0f,-0.02f }
#define YC_ASTEROID_BASIC { 0.0f, 0.05f,0.02f, 0.05f,0.0f, -0.05f,-0.02f,-0.05f }

class AsteroidBasic : public Asteroid
{
public:
	AsteroidBasic(float y_):
		Asteroid(y_, RAYON_BOX_ASTEROID_BASIC, LIFE_ASTEROID_BASIC, DAMAGE_ASTEROID_BASIC, 
			VITESSE_ASTEROID_BASIC, RED_ASTEROID_BASIC, GREEN_ASTEROID_BASIC, BLUE_ASTEROID_BASIC,
			XC_ASTEROID_BASIC, YC_ASTEROID_BASIC)
	{
	}

	int getLifeAtInit() {
		return LIFE_ASTEROID_BASIC;
	}
};

