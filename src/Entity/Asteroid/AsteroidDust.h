#pragma once
#include "Asteroid.h"

#define RAYON_BOX_ASTEROID_DUST 0.05f
#define LIFE_ASTEROID_DUST 50
#define DAMAGE_ASTEROID_DUST 25
#define VITESSE_ASTEROID_DUST -0.0023f
#define RED_ASTEROID_DUST 0.1f
#define GREEN_ASTEROID_DUST 0.1f
#define BLUE_ASTEROID_DUST 0.5f
#define XC_ASTEROID_DUST { -0.05f, -0.02f,0.0f, 0.02f,0.04f, 0.02f,0.0f,-0.02f }
#define YC_ASTEROID_DUST { 0.0f, 0.03f,0.02f, 0.03f,0.0f, -0.03f,-0.02f,-0.03f }

class AsteroidDust : public Asteroid
{
public:
	AsteroidDust(float y_) :
		Asteroid(y_, RAYON_BOX_ASTEROID_DUST, LIFE_ASTEROID_DUST, DAMAGE_ASTEROID_DUST, 
			VITESSE_ASTEROID_DUST, RED_ASTEROID_DUST, GREEN_ASTEROID_DUST, BLUE_ASTEROID_DUST,
			XC_ASTEROID_DUST, YC_ASTEROID_DUST)
	{}

	int getLifeAtInit() {
		return LIFE_ASTEROID_DUST;
	}
};