#include "Asteroid.h"

#define PI 3.14159265

void Asteroid::draw() {
	drawAsteroid();
	drawLife(getLifeAtInit());
}


void Asteroid::actionAtImpact(int damage_)
{
	life -= damage_;
}

void Asteroid::tick() {
	x += dX;
}

bool Asteroid::touchBorder() {
	return (x+rayonBox < -1.0f);
}

//A l'impact sur une tour, l'asteroid se d�truit
void Asteroid::hadHit()
{
	life = 0;
}

void Asteroid::drawAsteroid() {
	// export coords to pos x,y (center of asteroid)
	for (auto &i : xC) { i = i + x; }
	for (auto &i : yC) { i = i + y;	}
	GraphicPrimitives::drawOutlinedPolygone2D(xC, yC, red, green, blue);
	// recenter coords to origin
	for (auto &i : xC) {i = i - x;}
	for (auto &i : yC) {i = i - y;}
}

