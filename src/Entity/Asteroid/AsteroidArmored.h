#pragma once
#include "Asteroid.h"

#define RAYON_BOX_ASTEROID_ARMORED 0.05f
#define LIFE_ASTEROID_ARMORED 2500
#define DAMAGE_ASTEROID_ARMORED 250
#define VITESSE_ASTEROID_ARMORED -0.0002f
#define RED_ASTEROID_ARMORED 0.1f
#define GREEN_ASTEROID_ARMORED 0.6f
#define BLUE_ASTEROID_ARMORED 0.1f
#define XC_ASTEROID_ARMORED { -0.1f, -0.04f,0.0f, 0.04f,0.1f, 0.04f,0.0f,-0.04f }
#define YC_ASTEROID_ARMORED { 0.0f, 0.1f,0.04f, 0.1f,0.0f, -0.1f,-0.04f,-0.1f }

class AsteroidArmored : public Asteroid
{
public:
	AsteroidArmored(float y_) :
		Asteroid(y_, RAYON_BOX_ASTEROID_ARMORED, LIFE_ASTEROID_ARMORED, DAMAGE_ASTEROID_ARMORED, 
			VITESSE_ASTEROID_ARMORED,
			RED_ASTEROID_ARMORED, GREEN_ASTEROID_ARMORED, BLUE_ASTEROID_ARMORED,
			XC_ASTEROID_ARMORED, YC_ASTEROID_ARMORED)
	{}


	int getLifeAtInit() {
		return LIFE_ASTEROID_ARMORED;
	}

};

