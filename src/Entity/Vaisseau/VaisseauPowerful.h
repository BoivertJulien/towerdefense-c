#pragma once
#include "Vaisseau.h"
#include "../Missile/MissilePowerful.h"

#define FREQ_SHOT_VAISSEAU_POWERFUL 512
#define LIFE_AT_INIT_VAISSEAU_POWERFUL 150
#define IA_ESTIMATION_SPEED_VAISSEAU_POWERFUL 90
#define XC_VAISSEAU_POWERFUL { -0.1f, -0.04f,0.0f, 0.04f,0.1f, 0.04f,0.0f,-0.04f }
#define YC_VAISSEAU_POWERFUL { 0.0f, 0.1f,0.04f, 0.1f,0.0f, -0.1f,-0.04f,-0.1f  }
#define RAYON_BOX_VAISSEAU_POWERFUL 0.1f
#define RED_VAISSEAU_POWERFUL 0.2f
#define GREEN_VAISSEAU_POWERFUL 1.0f
#define BLUE_VAISSEAU_POWERFUL 0.2f

class VaisseauPowerful : public Vaisseau
{
public:
	VaisseauPowerful(float x_, float y_, std::vector<Missile * > * missiles_, BattleFieldCase * caseRef_, std::vector<Asteroid * > * asteroids_) :
		Vaisseau(x_, y_, RAYON_BOX_VAISSEAU_POWERFUL, LIFE_AT_INIT_VAISSEAU_POWERFUL, 
			missiles_, caseRef_, asteroids_, RED_VAISSEAU_POWERFUL, GREEN_VAISSEAU_POWERFUL, BLUE_VAISSEAU_POWERFUL,
			XC_VAISSEAU_POWERFUL,YC_VAISSEAU_POWERFUL, FREQ_SHOT_VAISSEAU_POWERFUL) {
	}
	~VaisseauPowerful() {}

	void shotOnTarget();
	void init_xC_yC();
	int getLifeAtInit() {
		return LIFE_AT_INIT_VAISSEAU_POWERFUL;
	}
	int getFreqShot() {
		return FREQ_SHOT_VAISSEAU_POWERFUL;
	}
};
