#include "VaisseauPowerful.h"


void VaisseauPowerful::shotOnTarget() {

	float relativeEstimateX = (eyesOn->x + eyesOn->dX*IA_ESTIMATION_SPEED_VAISSEAU_POWERFUL) - x;
	float relativeEstimateY = (eyesOn->y + eyesOn->dY*IA_ESTIMATION_SPEED_VAISSEAU_POWERFUL) - y;

	missiles->push_back(new MissilePowerful(x, y,
		relativeEstimateX / IA_ESTIMATION_SPEED_VAISSEAU_POWERFUL,
		relativeEstimateY / IA_ESTIMATION_SPEED_VAISSEAU_POWERFUL));
}

void VaisseauPowerful::init_xC_yC()
{
	xC = XC_VAISSEAU_POWERFUL;
	yC = YC_VAISSEAU_POWERFUL;
}
