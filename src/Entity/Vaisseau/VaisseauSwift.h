#pragma once
#include "Vaisseau.h"
#include "../Missile/MissileSwift.h"

#define FREQ_SHOT_VAISSEAU_SWIFT 64
#define LIFE_AT_INIT_VAISSEAU_SWIFT 25
#define IA_ESTIMATION_SPEED_VAISSEAU_SWIFT 100
#define XC_VAISSEAU_SWIFT { -0.03f, -0.01f,0.0f, 0.01f,0.03f, 0.01f,0.0f,-0.01f }
#define YC_VAISSEAU_SWIFT { 0.0f, 0.03f,0.01f, 0.03f,0.0f, -0.03f,-0.01f,-0.03f }
#define RAYON_BOX_VAISSEAU_SWIFT 0.03f
#define RED_VAISSEAU_SWIFT 0.2f
#define GREEN_VAISSEAU_SWIFT 0.2f
#define BLUE_VAISSEAU_SWIFT 1.0f

class VaisseauSwift : public Vaisseau
{
public:
	VaisseauSwift(float x_, float y_, std::vector<Missile * > * missiles_, BattleFieldCase * caseRef_, std::vector<Asteroid * > * asteroids_) :
		Vaisseau(x_, y_, RAYON_BOX_VAISSEAU_SWIFT, LIFE_AT_INIT_VAISSEAU_SWIFT, 
			missiles_, caseRef_, asteroids_, RED_VAISSEAU_SWIFT, GREEN_VAISSEAU_SWIFT, BLUE_VAISSEAU_SWIFT,
			XC_VAISSEAU_SWIFT, YC_VAISSEAU_SWIFT, FREQ_SHOT_VAISSEAU_SWIFT)
	{
	}

	~VaisseauSwift() {}

	void shotOnTarget();
	void init_xC_yC();
	int getLifeAtInit() {
		return LIFE_AT_INIT_VAISSEAU_SWIFT;
	}
	int getFreqShot() {
		return FREQ_SHOT_VAISSEAU_SWIFT;
	}
};
