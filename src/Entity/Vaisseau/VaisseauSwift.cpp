#include "VaisseauSwift.h"


void VaisseauSwift::shotOnTarget() {
	float relativeEstimateX = (eyesOn->x + eyesOn->dX*IA_ESTIMATION_SPEED_VAISSEAU_SWIFT) - x;
	float relativeEstimateY = (eyesOn->y + eyesOn->dY*IA_ESTIMATION_SPEED_VAISSEAU_SWIFT) - y;

	missiles->push_back(new MissileSwift(x, y,
		relativeEstimateX / IA_ESTIMATION_SPEED_VAISSEAU_SWIFT,
		relativeEstimateY / IA_ESTIMATION_SPEED_VAISSEAU_SWIFT));
}

void VaisseauSwift::init_xC_yC()
{
	xC = XC_VAISSEAU_SWIFT;
	yC = YC_VAISSEAU_SWIFT;
}