#pragma once
#include "../Entity.h"
#include "../Missile/Missile.h"
#include "../Asteroid/Asteroid.h"
#include "../../Container/Case/BattleFieldCase.h"



class Vaisseau : public Entity {
    
public:
	std::vector<Missile * > * missiles;
	std::vector<Asteroid * > * asteroids;

	int freqShot; //variable decrement� depuis la globale FREQ_SHOT
	BattleFieldCase * caseRef;
	Asteroid * eyesOn;

	float degree; // on calcule rotation que si la direction a chang�
	std::vector<float> xC;
	std::vector<float> yC;

	Vaisseau(float x_, float y_, float rayonBox_, int life_, std::vector<Missile * > * missiles_, BattleFieldCase * caseRef_, std::vector<Asteroid * > * asteroids_, float red_, float green_, float blue_, std::vector<float> xC_, std::vector<float> yC_,int freqShot_) :
		Entity(x_, y_, rayonBox_, life_, 0, 0.0f, 0.0f, red_, green_, blue_),
		missiles(missiles_), freqShot(freqShot_),caseRef(caseRef_),eyesOn(nullptr),asteroids(asteroids_),
		degree(0), xC(xC_),yC(yC_)
	{
		rotate();
	}
	~Vaisseau() {
		// On ne "delete" pas les pointeurs (attributs de la classe) car ceux-ci continuent d'exister dans l'environnement du jeu
	}


	//Fonction abstract de la classe Entity
	void tick();
	bool isFocus();
	void actionAtImpact(int damage_);
	void setNewCible();
	void draw();

	virtual void shotOnTarget()=0;
	virtual void init_xC_yC()=0;
	virtual int getFreqShot()=0;
	virtual int getLifeAtInit()=0;

	bool needNewRotation(Asteroid * eyesOn_);

	void rotate();

	void drawVaisseau();

	void rePlaceForBattle();

	BattleFieldCase * getCaseRef() {
		return caseRef;
	}

private : 
	//Une tour n'inflige pas d'attaque direct
	void hadHit();

};