#include "Vaisseau.h"

#define PI 3.14159265

void Vaisseau::draw(){
	drawVaisseau();
	drawLife(getLifeAtInit());
}

void Vaisseau::tick(){
	freqShot--;
	if (freqShot <= 0) {
		setNewCible();
		if (!eyesOn == NULL) {
			freqShot = getFreqShot();
			shotOnTarget();
		}
	}
	if (needNewRotation(eyesOn)) { // met aussi a jour le degre
		init_xC_yC(); //set the xC and yC coords to the origin with defaults form
		rotate();
	}
	return;
}
bool Vaisseau::isFocus()
{
	return (eyesOn != nullptr);
}
void Vaisseau::hadHit()
{
	return;
}
void Vaisseau::actionAtImpact(int damage_)
{
	life -= damage_;
}

void Vaisseau::setNewCible()
{
	if (asteroids->size() > 0) {
		int indexNear = -1; // vaut -1 si pas d'ennemi a proximit�
		float tmpDist;
		Asteroid * tmp;
		float distNear = 0.8f; // init a la distance maximale pour etre cibl�
		for (unsigned int i = 0; i < asteroids->size(); i++) {
			tmp = (*asteroids)[i];
			tmpDist = getDist(tmp->x, tmp->y);
			if ((tmpDist < distNear) && (tmpDist < 0.8f)) {
				indexNear = i;
				distNear = tmpDist;
			}
		}
		if (indexNear >= 0)	eyesOn = (*asteroids)[indexNear];
		else eyesOn = nullptr;
	}
	else eyesOn = nullptr;
}

bool Vaisseau::needNewRotation(Asteroid * eyesOn_) {
	float newDegree;
	if (eyesOn_ == NULL) newDegree = 0.0f;
	else {
		newDegree = atan2(eyesOn_->x - x, eyesOn_->y - y) * 180 / PI;
		if (newDegree > 0) newDegree = 360 - newDegree;
		else newDegree = -newDegree;
		newDegree = ((int)newDegree + 90) % 360;
		newDegree = newDegree / 180 * PI;
	}
	if (newDegree != degree) {
		//On met a jour le nouveau degres
		degree = newDegree;
		return true;
	}
	return false;
}

void Vaisseau::rotate() {

	float newX, newY;
	for (unsigned int i = 0; i < xC.size(); i++) {
		newX = (xC)[i] * cos(degree) - (yC)[i] * sin(degree);
		newY = (yC)[i] * cos(degree) + (xC)[i] * sin(degree);
		xC[i] = newX;
		yC[i] = newY;
	}
	//Move from origin to x,y pos (center of the ship)
	for (auto &i : xC) {
		i = i + x;
	}
	for (auto &i : yC) {
		i = i + y;
	}
}

void Vaisseau::drawVaisseau() {
	GraphicPrimitives::drawOutlinedPolygone2D(xC, yC, red, green, blue);
}

void Vaisseau::rePlaceForBattle() {
	init_xC_yC();
	degree = 0.0f;
	eyesOn = nullptr;
	rotate();
}