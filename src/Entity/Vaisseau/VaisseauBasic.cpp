#include "VaisseauBasic.h"

void VaisseauBasic::shotOnTarget() {

	float relativeEstimateX = (eyesOn->x + eyesOn->dX*IA_ESTIMATION_SPEED_VAISSEAU_BASIC) - x;
	float relativeEstimateY = (eyesOn->y + eyesOn->dY*IA_ESTIMATION_SPEED_VAISSEAU_BASIC) - y;

	missiles->push_back(new MissileBasic(x, y,
		relativeEstimateX / IA_ESTIMATION_SPEED_VAISSEAU_BASIC,
		relativeEstimateY / IA_ESTIMATION_SPEED_VAISSEAU_BASIC));
}

void VaisseauBasic::init_xC_yC()
{
	xC = XC_VAISSEAU_BASIC;
	yC = YC_VAISSEAU_BASIC;
}
