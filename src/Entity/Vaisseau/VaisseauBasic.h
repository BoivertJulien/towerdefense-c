#pragma once
#include "Vaisseau.h"
#include "../Missile/MissileBasic.h"

#define FREQ_SHOT_VAISSEAU_BASIC 248
#define LIFE_AT_INIT_VAISSEAU_BASIC 50
#define IA_ESTIMATION_SPEED_VAISSEAU_BASIC 128
#define XC_VAISSEAU_BASIC { -0.05f, -0.02f,0.0f, 0.02f,0.05f, 0.02f,0.0f,-0.02f }
#define YC_VAISSEAU_BASIC { 0.0f, 0.05f,0.02f, 0.05f,0.0f, -0.05f,-0.02f,-0.05f }
#define RAYON_BOX_VAISSEAU_BASIC 0.05f
#define RED_VAISSEAU_BASIC 1.0f
#define GREEN_VAISSEAU_BASIC 0.2f
#define BLUE_VAISSEAU_BASIC 0.2f

class VaisseauBasic : public Vaisseau
{
public:
	VaisseauBasic(float x_, float y_, std::vector<Missile * > * missiles_, BattleFieldCase * caseRef_, std::vector<Asteroid * > * asteroids_) :
		Vaisseau(x_, y_, RAYON_BOX_VAISSEAU_BASIC, LIFE_AT_INIT_VAISSEAU_BASIC,
			missiles_, caseRef_, asteroids_, RED_VAISSEAU_BASIC, GREEN_VAISSEAU_BASIC, BLUE_VAISSEAU_BASIC,
			XC_VAISSEAU_BASIC, YC_VAISSEAU_BASIC, FREQ_SHOT_VAISSEAU_BASIC)
	{
	}

	~VaisseauBasic() {}
	
	void shotOnTarget();
	void init_xC_yC();
	int getLifeAtInit() {
		return LIFE_AT_INIT_VAISSEAU_BASIC;
	}
	int getFreqShot() {
		return FREQ_SHOT_VAISSEAU_BASIC;
	}
};