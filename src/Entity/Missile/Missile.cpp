#include "Missile.h"
#include <cmath>

#define PI 3.14159265

void Missile::tick() {
	x += dX;
	y += dY;
}

void Missile::draw()
{
	GraphicPrimitives::drawFillTriangle2D(x + (x1*rayonBox), y + (y1*rayonBox), x + (x2*(rayonBox / 2)), y + (y2*(rayonBox / 2)), x + (x3*(rayonBox)), y + (y3*(rayonBox / 2)), red, green, blue, 1.0);
}

//Un missile ne peut pas etre cibl� par une attaque
void Missile::actionAtImpact(int damage_)
{
	return;
}

void Missile::hadHit()
{
	life -= 1;
}

// Detection d'obstacles sorti de la zone, pour leur suppression
bool Missile::touchBorder() { 
	return ((x + rayonBox < -1.0f || x - rayonBox > 1.0f) || (y + rayonBox < -0.6f || y - rayonBox > 0.8f));
}

void Missile::rotate() {
	float newDegree = atan2(dX, dY) * 180 / PI;

	if (newDegree > 0) newDegree = 360 - newDegree;
	else newDegree = -newDegree;

	newDegree = ((int)newDegree + 90) % 360;
	newDegree = newDegree / 180 * PI;

	degree = newDegree;
	x1 = cos(degree);
	y1 = sin(degree);
	// Pour les 2 autres points, on les faits tourner du meme degr� et plus : 2/3PI et 4/3PI
	x2 = cos(degree + 2 * PI / 3);
	y2 = sin(degree + 2 * PI / 3);
	x3 = cos(degree + 4 * PI / 3);
	y3 = sin(degree + 4 * PI / 3);
}
