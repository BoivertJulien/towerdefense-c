#pragma once
#include "Missile.h"

#define RAYON_BOX_MISSILE_POWERFUL 0.04f
#define LIFE_MISSILE_POWERFUL 3
#define DAMAGE_MISSILE_POWERFUL 200
#define RED_MISSILE_POWERFUL 0.2f
#define GREEN_MISSILE_POWERFUL 1.0f
#define BLUE_MISSILE_POWERFUL 0.2f

class MissilePowerful : public Missile
{
public:
	MissilePowerful(float x_, float y_, float dX_, float dY_) :
		Missile(x_, y_, RAYON_BOX_MISSILE_POWERFUL, LIFE_MISSILE_POWERFUL, DAMAGE_MISSILE_POWERFUL,
			dX_, dY_, RED_MISSILE_POWERFUL, GREEN_MISSILE_POWERFUL, BLUE_MISSILE_POWERFUL)
	{}
	~MissilePowerful(){}

};