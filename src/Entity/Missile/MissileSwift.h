#pragma once
#include "Missile.h"

#define RAYON_BOX_MISSILE_SWIFT 0.01f
#define LIFE_MISSILE_SWIFT 1
#define DAMAGE_MISSILE_SWIFT 20
#define RED_MISSILE_SWIFT 0.2f
#define GREEN_MISSILE_SWIFT 0.2f
#define BLUE_MISSILE_SWIFT 1.0f

class MissileSwift : public Missile
{
public:
	MissileSwift(float x_, float y_, float dX_, float dY_) :
		Missile(x_, y_, RAYON_BOX_MISSILE_SWIFT, LIFE_MISSILE_SWIFT, DAMAGE_MISSILE_SWIFT, 
			dX_, dY_, RED_MISSILE_SWIFT, GREEN_MISSILE_SWIFT, BLUE_MISSILE_SWIFT)
	{}
	~MissileSwift() {}

};