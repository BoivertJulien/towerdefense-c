#pragma once
#include "../Entity.h"

class Missile : public Entity {

	public:

		// pour rapidit� du code :
		float degree; // on calcule rotation que si la direction a chang�
		float x1, y1, x2, y2, x3, y3;


		Missile(float x_, float y_, float rayonBox_, int life_, int damage_, float dX_, float dY_, float red_, float green_, float blue_) : 
			Entity(x_, y_, rayonBox_, life_, damage_, dX_,dY_,red_,green_,blue_),degree(0)
		{
			rotate();
		}

		//Fonction h�rit�e de la classe Entity
		void draw();
		void tick();
		void hadHit();

		//Fonction perso
		bool touchBorder();
		void rotate();

private :
		void actionAtImpact(int damage_); // inutile pour la classe Missile
};