#pragma once
#include "Missile.h"

#define RAYON_BOX_MISSILE_BASIC 0.02f
#define LIFE_MISSILE_BASIC 2
#define DAMAGE_MISSILE_BASIC 20
#define RED_MISSILE_BASIC 1.0f
#define GREEN_MISSILE_BASIC 0.2f
#define BLUE_MISSILE_BASIC 0.2f

class MissileBasic : public Missile
{
public:
	MissileBasic(float x_, float y_,float dX_,float dY_) :
		Missile(x_, y_, RAYON_BOX_MISSILE_BASIC, LIFE_MISSILE_BASIC, DAMAGE_MISSILE_BASIC, 
			dX_,  dY_, RED_MISSILE_BASIC, GREEN_MISSILE_BASIC, BLUE_MISSILE_BASIC)
	{}

	~MissileBasic() {}
};

