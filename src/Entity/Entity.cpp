#include "Entity.h"


bool Entity::receiveImpact(Entity * ent)
{
	float dist = sqrt((ent->x - x)*(ent->x - x) + (ent->y - y)*(ent->y - y));
	return (dist < rayonBox + ent->rayonBox); //calcul de la box de collision
}

float Entity::getDist(float x_, float y_) {
	return sqrt((x_ - x)*(x_ - x) + (y_ - y)*(y_ - y));
}

void Entity::drawLife(int initLifePoint) {
	GraphicPrimitives::drawFillRect2D(x - 0.05f, y - rayonBox, 2 * 0.05f, 0.01f, 1.0f, 0.0f, 0.0f);
	GraphicPrimitives::drawFillRect2D(x - 0.05f, y - rayonBox, ((2 * 0.05f) / initLifePoint)*life, 0.01f, 0.0f, 1.0f, 0.0f);
}